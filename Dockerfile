FROM python:3.8-slim-buster

WORKDIR /app
COPY requirements.txt requirements.txt
ARG SECRET_NUMBER_ARG
ENV SECRET_NUMBER_ARG=$SECRET_NUMBER_ARG


RUN pip3 install -r requirements.txt
COPY . .

CMD [ "python3", "app.py"]
